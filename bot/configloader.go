package bot

import (
	"encoding/json"
	"flag"
	"os"
)

var Config = loadConfig()

type Configuration struct {
	Periods struct {
		Attackcheckperiod string `json:"attackcheckperiod"`
		Dataloadperiod    string `json:"dataloadperiod"`
	} `json:"Periods"`
	Session struct {
		Cookies  string `json:"cookies"`
		Server   string `json:"server"`
		Token    string `json:"token"`
		URL      string `json:"url"`
		TGApiKey string `json:"tgapikey"`
	} `json:"Session"`
}

func loadConfig() Configuration {
	var configpath = flag.String("configpath", "./bot/config.json", "path ot config file")
	flag.Parse()
	configfile, _ := os.Open(*configpath)
	defer configfile.Close()
	decoder := json.NewDecoder(configfile)
	config := Configuration{}
	err := decoder.Decode(&config)
	if err != nil {
		return Configuration{}
	}
	return config
}
