package bot

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"tkkb/models"
)

var Playerdata = getall.PlayerData{}
var Herodata = getall.HeroData{}
var Structures = []getall.AllBuildings{}
var Attacks = []string{}

type ReqOptions struct {
	Req getall.RequestOptions
}

func (opt ReqOptions) MakeRequest(server, url string) ([]byte, error) {
	fullurl := "https://" + server + "." + url + "/api/?c=" + opt.Req.Data.Controller + "&a=" + opt.Req.Data.Action + "&t" + strconv.FormatInt(time.Now().Unix(), 10)
	jsonStr, err := json.Marshal(opt.Req.Data)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(opt.Req.Method, fullurl, bytes.NewBuffer(jsonStr))
	getcookie := parsecookie(Config.Session.Cookies)
	for _, cookie := range getcookie {
		req.AddCookie(cookie)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func parsecookie(cookies string) []*http.Cookie {
	rawCookies := cookies
	rawRequest := fmt.Sprintf("GET / HTTP/1.0\r\nCookie: %s\r\n\r\n", rawCookies)

	req, err := http.ReadRequest(bufio.NewReader(strings.NewReader(rawRequest)))

	if err == nil {
		cookies := req.Cookies()
		return cookies
	}
	return nil
}

func LoadData(opt ReqOptions, token, server string) (getall.PlayerData, getall.HeroData, []getall.AllBuildings, []string, error) {
	var playerdata = getall.PlayerData{}
	var herodata = getall.HeroData{}
	var structures = []getall.AllBuildings{}
	body, err := opt.MakeRequest(server, Config.Session.URL)
	if err != nil {
		return getall.PlayerData{}, getall.HeroData{}, []getall.AllBuildings{}, []string{}, err
	}
	playerdata, herodata, err = parseData(body)
	if err != nil {
		return getall.PlayerData{}, getall.HeroData{}, []getall.AllBuildings{}, []string{}, err
	}
	structures, err = parseBuildingsData(body, Playerdata)
	if err != nil {
		return getall.PlayerData{}, getall.HeroData{}, []getall.AllBuildings{}, []string{}, err
	}
	attacks, err := findAttacks(body, playerdata)
	return playerdata, herodata, structures, attacks, nil
}

func parseData(body []byte) (getall.PlayerData, getall.HeroData, error) {
	var cache map[string]*json.RawMessage
	var cacheitems []map[string]*json.RawMessage

	err := json.Unmarshal(body, &cache)
	if err != nil {
		return getall.PlayerData{}, getall.HeroData{}, err
	}
	err = json.Unmarshal(*cache["cache"], &cacheitems)
	if err != nil {
		return getall.PlayerData{}, getall.HeroData{}, err
	}
	for _, cacheitem := range cacheitems {
		if strings.Contains(string(*cacheitem["name"]), "Player:") {

			err = json.Unmarshal(*cacheitem["data"], &Playerdata)
			if err != nil {
				return getall.PlayerData{}, getall.HeroData{}, err
			}
		}
		if strings.Contains(string(*cacheitem["name"]), "Hero:") {

			err = json.Unmarshal(*cacheitem["data"], &Herodata)
			if err != nil {
				return getall.PlayerData{}, getall.HeroData{}, err
			}
		}
	}
	return Playerdata, Herodata, nil
}

func parseBuildingsData(body []byte, playerdata getall.PlayerData) ([]getall.AllBuildings, error) {
	var cache map[string]*json.RawMessage
	var cacheitems []map[string]*json.RawMessage

	var allbuildings []getall.AllBuildings
	err := json.Unmarshal(body, &cache)
	if err != nil {
		return []getall.AllBuildings{}, err
	}
	err = json.Unmarshal(*cache["cache"], &cacheitems)
	if err != nil {
		return []getall.AllBuildings{}, err
	}
	for _, village := range playerdata.Villages {
		var buildings getall.AllBuildings
		for _, cacheitem := range cacheitems {
			if strings.Contains(string(*cacheitem["name"]), "Collection:Building:"+village.VillageID) {
				var buildingsdata []getall.CollectionBuilding
				var buildingscache map[string]*json.RawMessage
				err = json.Unmarshal(*cacheitem["data"], &buildingscache)
				if err != nil {
					return []getall.AllBuildings{}, err
				}
				err = json.Unmarshal(*buildingscache["cache"], &buildingsdata)
				if err != nil {
					return []getall.AllBuildings{}, err
				}
				buildings.VillageID = village.VillageID
				buildings.CollectionBuildings = buildingsdata
			}
		}
		allbuildings = append(allbuildings, buildings)
	}
	return allbuildings, nil
}

func findAttacks(body []byte, playerdata getall.PlayerData) ([]string, error) {
	var cache map[string]*json.RawMessage
	var cacheitems []map[string]*json.RawMessage
	var unitscache map[string]*json.RawMessage
	// var movingunits []getall.MovingUnits
	var movingunits []interface{}
	err := json.Unmarshal(body, &cache)
	if err != nil {
		return []string{}, err
	}
	err = json.Unmarshal(*cache["cache"], &cacheitems)
	if err != nil {
		return []string{}, err
	}
	for _, village := range playerdata.Villages {
		for _, cacheitem := range cacheitems {
			if strings.Contains(string(*cacheitem["name"]), "Collection:Troops:moving:"+village.VillageID) {
				err = json.Unmarshal(*cacheitem["data"], &unitscache)
				if err != nil {
					return []string{}, err
				}
				err = json.Unmarshal(*unitscache["cache"], &movingunits)
				if err != nil {
					return []string{}, err
				}
				for _, movingunit := range movingunits {
					movingunitMap := movingunit.(map[string]interface{})
					movingunitMapData := movingunitMap["data"].(map[string]interface{})
					// movingunitMapDataUnits := movingunitMapData["units"].(map[string]interface{})
					movingunitMapDataMovement := movingunitMapData["movement"].(map[string]interface{})
					// movingunitMapDataMovementResources := movingunitMapDataMovement["resources"].(map[string]interface{})
					if (movingunitMapDataMovement["movementType"] == "3" || movingunitMapDataMovement["movementType"] == "4") && movingunitMapDataMovement["villageIdTarget"] == village.VillageID {
						attacktimeunix, err := strconv.ParseInt(movingunitMapDataMovement["timeFinish"].(string), 10, 64)
						if err != nil {
							return []string{}, err
						}
						attacktime := time.Unix(attacktimeunix, 0)
						req, err := http.Get("https://api.telegram.org/bot" + Config.Session.TGApiKey + "/sendMessage?chat_id=64610554&text=ALARM+" + village.Name + " at " + attacktime.String() + " attacker: " + movingunitMapData["playerName"].(string) + ", from " + movingunitMapDataMovement["villageIdStart"].(string))
						if err != nil {
							return []string{}, err
						}
						defer req.Body.Close()
						Attacks = append(Attacks, village.VillageID)
					}
				}
				// for _, movingunit := range movingunits {
				// 	if (movingunit.Data.Movement.MovementType == "3" || movingunit.Data.Movement.MovementType == "4") && movingunit.Data.Movement.VillageIDTarget == village.VillageID {
				// 		attacktimeunix, err := strconv.ParseInt(movingunit.Data.Movement.TimeFinish, 10, 64)
				// 		if err != nil {
				// 			return []string{}, err
				// 		}
				// 		attacktime := time.Unix(attacktimeunix, 0)
				// 		req, err := http.Get("https://api.telegram.org/bot" + Config.Session.TGApiKey + "/sendMessage?chat_id=64610554&text=ALARM+" + village.Name + " at " + attacktime.String() + " attacker: " + movingunit.Data.PlayerName + ", from " + movingunit.Data.Movement.VillageIDStart)
				// 		if err != nil {
				// 			return []string{}, err
				// 		}
				// 		defer req.Body.Close()
				// 		attacks = append(attacks, village.VillageID)
				// 	}
				// }
			}
		}
	}
	return Attacks, nil
}

func AutoAdventures(opt ReqOptions, token, server string) error {
	_, err := opt.MakeRequest(server, Config.Session.URL)
	if err != nil {
		return err
	}
	return nil
}

func GetFL(opt ReqOptions, token, server string) ([]interface{}, error) {
	body, err := opt.MakeRequest(server, Config.Session.URL)
	if err != nil {
		return nil, err
	}
	var cache map[string]*json.RawMessage
	var cacheitems []interface{}
	err = json.Unmarshal(body, &cache)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(*cache["cache"], &cacheitems)
	if err != nil {
		return nil, err
	}
	return cacheitems, nil
}
