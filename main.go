package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
	"tkkb/bot"
	"tkkb/models"

	"github.com/marcusolsson/tui-go"
)

var config = bot.Config

var logo = `_________ _        ______   _______ _________
\__   __/| \    /\(  ___ \ (  ___  )\__   __/
   ) (   |  \  / /| (   ) )| (   ) |   ) (   
   | |   |  (_/ / | (__/ / | |   | |   | |   
   | |   |   _ (  |  __ (  | |   | |   | |   
   | |   |  ( \ \ | (  \ \ | |   | |   | |   
   | |   |  /  \ \| )___) )| (___) |   | |   
   )_(   |_/    \/|/ \___/ (_______)   )_(   
                                             `

var dataloadperiod, _ = time.ParseDuration(config.Periods.Dataloadperiod)
var token = config.Session.Token
var url = config.Session.URL
var cookies = config.Session.Cookies
var server = config.Session.Server

var dataLabelslength int
var heroLabelslength int
var flListlength int

var dataLabels *tui.Box
var heroLabels *tui.Box
var flList *tui.Box

var wg sync.WaitGroup

type uiTab struct {
	label *tui.Label
	view  tui.Widget
}

func newTabWidget(views ...*uiTab) *tabWidget {
	topbar := tui.NewHBox(tui.NewLabel("> "))
	topbar.SetSizePolicy(tui.Minimum, tui.Maximum)
	view := &tabWidget{views: views}

	for i := 0; i < len(views); i++ {
		topbar.Append(views[i].label)
	}

	topbar.Append(tui.NewSpacer())
	view.style()

	vbox := tui.NewVBox(topbar, views[0].view)
	vbox.SetSizePolicy(tui.Maximum, tui.Preferred)
	view.Box = vbox
	return view
}

type tabWidget struct {
	*tui.Box

	views  []*uiTab
	active int
}

func (t *tabWidget) OnKeyEvent(ev tui.KeyEvent) {
	switch ev.Key {
	case tui.KeyRight:
		t.Next()
	case tui.KeyLeft:
		t.Previous()
	}

	t.Box.OnKeyEvent(ev)
}

func (t *tabWidget) setView(view tui.Widget) {
	t.Box.Remove(1)
	t.Box.Append(view)
}

func (t *tabWidget) style() {
	for i := 0; i < len(t.views); i++ {
		if i == t.active {
			t.views[i].label.SetStyleName("tab-selected")
			continue
		}
		t.views[i].label.SetStyleName("tab")
	}
}

func (t *tabWidget) Next() {
	t.active = clamp(t.active+1, 0, len(t.views)-1)
	t.style()
	t.setView(t.views[t.active].view)
}

func (t *tabWidget) Previous() {
	t.active = clamp(t.active-1, 0, len(t.views)-1)
	t.style()
	t.setView(t.views[t.active].view)
}

func clamp(n, min, max int) int {
	if n < min {
		return max
	}
	if n > max {
		return min
	}
	return n
}

func main() {

	// Logs
	history := tui.NewVBox()
	historyScroll := tui.NewScrollArea(history)
	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetSizePolicy(tui.Expanding, tui.Expanding)
	historyBox.SetBorder(true)
	historyBox.SetTitle("Logs")
	// Logs End

	// Side menu
	startButton := tui.NewButton("[Start]")
	startButton.OnActivated(func(b *tui.Button) {
		go loaddata(history, server)
	})
	stopButton := tui.NewButton("[Stop]")
	stopButton.OnActivated(func(b *tui.Button) {
		os.Exit(0)
	})
	buttons := tui.NewVBox(
		tui.NewPadder(1, 0, startButton),
		tui.NewPadder(1, 0, stopButton),
		tui.NewSpacer(),
	)
	menuBox := tui.NewVBox(buttons)
	menuBox.SetBorder(true)
	menuBox.SetTitle("Menu")
	// Side menu end

	// Villages data
	dataLabels = tui.NewVBox()
	dataLabels.SetSizePolicy(tui.Expanding, tui.Maximum)
	dataScroll := tui.NewScrollArea(dataLabels)
	dataBox := tui.NewVBox(dataScroll)
	dataBox.SetSizePolicy(tui.Expanding, tui.Expanding)
	dataBox.SetBorder(true)
	dataBox.SetTitle("Data")
	datachat := tui.NewVBox(dataBox)
	datachat.SetSizePolicy(tui.Expanding, tui.Expanding)
	// Villages data end

	// Hero data
	heroLabels = tui.NewVBox()
	heroLabels.SetBorder(true)
	heroLabels.SetTitle("Hero Data")
	heroLabels.SetSizePolicy(tui.Expanding, tui.Maximum)
	// Hero data end

	// Farm List controller
	flList = tui.NewVBox()
	flList.SetTitle("Farm Lists")
	flList.SetBorder(true)
	flList.SetSizePolicy(tui.Expanding, tui.Maximum)
	// Farm List controller end

	// Login Tab
	userserver := tui.NewEntry()
	username := tui.NewEntry()
	password := tui.NewEntry()
	password.SetEchoMode(tui.EchoModePassword)
	form := tui.NewGrid(0, 0)
	form.AppendRow(tui.NewLabel("Server"), tui.NewLabel("Username"), tui.NewLabel("Password"))
	form.AppendRow(userserver, username, password)
	loginstatus := tui.NewStatusBar("Ready.")
	loginbtn := tui.NewButton("[Login]")
	loginbtn.OnActivated(func(b *tui.Button) {
		loginUser(history, userserver, username, password)
		loginstatus.SetText("Logged in.")
	})
	authbuttons := tui.NewHBox(
		tui.NewSpacer(),
		tui.NewPadder(1, 0, loginbtn),
	)
	window := tui.NewVBox(
		tui.NewPadder(10, 1, tui.NewLabel(logo)),
		tui.NewPadder(25, 0, tui.NewLabel("Welcome to TKBot!")),
		tui.NewPadder(1, 1, form),
		authbuttons,
		loginstatus,
	)
	window.SetBorder(true)
	window.SetTitle("Login Form")
	wrapper := tui.NewVBox(
		tui.NewSpacer(),
		window,
		tui.NewSpacer(),
	)
	content := tui.NewHBox(tui.NewSpacer(), wrapper, tui.NewSpacer())
	logintabcontent := tui.NewVBox(
		content,
	)
	// Login Tab end

	// Main tab instructions
	instructions := tui.NewVBox()
	instructions.SetTitle("Instructions")
	instructions.SetBorder(true)
	instructions.Append(tui.NewVBox(
		tui.NewLabel(logo),
		tui.NewLabel("Left/Right keys to switch tabs"),
		tui.NewLabel("Up/Down keys to scroll Logs"),
		tui.NewLabel("A key to autoscroll Logs to bottom"),
		tui.NewLabel("T key to scroll Logs to top"),
		tui.NewLabel("B key to scroll Logs to bottom"),
		tui.NewLabel("Tab key to select buttons"),
		tui.NewLabel("Esq/Q keys to shutdown bot"),
		tui.NewLabel("Enter key to activate button"),
		tui.NewSpacer(),
	))
	// Main tab instructions end

	// Main gui logic
	villagesData := tui.NewHBox(menuBox, datachat)
	villagesData.SetTitle("Villages")
	villagesData.SetBorder(true)
	tabLayout := newTabWidget(
		&uiTab{label: tui.NewLabel("[Instructions] "), view: instructions},
		&uiTab{label: tui.NewLabel("[Login] "), view: logintabcontent},
		&uiTab{label: tui.NewLabel(" [Villages] "), view: villagesData},
		&uiTab{label: tui.NewLabel(" [Farm Lists] "), view: flList},
		&uiTab{label: tui.NewLabel(" [Hero] "), view: heroLabels},
		&uiTab{label: tui.NewLabel(" [Logs] "), view: historyBox},
	)
	ui, err := tui.New(tabLayout)
	if err != nil {
		req, _ := http.Get("https://api.telegram.org/bot" + config.Session.TGApiKey + "/sendMessage?chat_id=64610554&text=ERROR" + fmt.Sprintf("%s", err))
		defer req.Body.Close()
	}
	tui.DefaultFocusChain.Set(userserver, username, password, loginbtn, startButton, stopButton)
	ui.SetKeybinding("Esc", func() { ui.Quit() })
	ui.SetKeybinding("q", func() { ui.Quit() })
	ui.SetKeybinding("Up", func() { historyScroll.Scroll(0, -1) })
	ui.SetKeybinding("Down", func() { historyScroll.Scroll(0, 1) })
	ui.SetKeybinding("Up", func() { dataScroll.Scroll(0, -1) })
	ui.SetKeybinding("Down", func() { dataScroll.Scroll(0, 1) })
	ui.SetKeybinding("a", func() { historyScroll.SetAutoscrollToBottom(true) })
	ui.SetKeybinding("t", func() { historyScroll.ScrollToTop() })
	ui.SetKeybinding("b", func() { historyScroll.ScrollToBottom() })
	if err := ui.Run(); err != nil {
		req, _ := http.Get("https://api.telegram.org/bot" + config.Session.TGApiKey + "/sendMessage?chat_id=64610554&text=ERROR" + fmt.Sprintf("%s", err))
		defer req.Body.Close()
	}
	// Main gui logic end
}

func writelog(history *tui.Box, data string) {
	history.Append(tui.NewHBox(
		tui.NewLabel(time.Now().Local().String()),
		tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", "tkbot"))),
		tui.NewLabel(data),
		tui.NewSpacer(),
	))
}

func loaddata(history *tui.Box, server string) {
	for {
		writelog(history, "Data loading...")
		var getalloption bot.ReqOptions
		getalloption.Req.Method = "POST"
		getalloption.Req.Data.Session = token
		getalloption.Req.Data.Action = "getAll"
		getalloption.Req.Data.Controller = "player"
		playerdata, herodata, structures, attacks, err := bot.LoadData(getalloption, token, server)
		if err != nil {
			writelog(history, fmt.Sprintf("Data loaded with errors: %s", err))
		} else {
			writelog(history, fmt.Sprintf("Data loaded with success. Next run at: %s", time.Now().Add(dataloadperiod).Local().String()))
			if dataLabelslength != 0 {
				for i := 0; i <= dataLabelslength; i++ {
					dataLabels.Remove(0)
				}
			}
			if heroLabelslength != 0 {
				for i := 0; i <= heroLabelslength; i++ {
					heroLabels.Remove(0)
				}
			}
			if flListlength != 0 {
				for i := 0; i <= flListlength; i++ {
					flList.Remove(0)
				}
			}
			for _, village := range playerdata.Villages {
				// woodcap, err := strconv.Atoi(village.StorageCapacity["1"].(string))
				// if err != nil {
				// 	writelog(history, fmt.Sprintf("Data loaded with errors: %s", err))
				// }
				// resbar := tui.NewProgress(100)
				// woodcur := (int(village.Storage["1"].(float64)) * 100) / woodcap
				// resbar.SetCurrent(woodcur)
				dataLabels.Append(tui.NewHBox(
					tui.NewLabel("Village Name: "+village.Name+" ("+village.VillageID+")"),
					tui.NewLabel("Resources: "+fmt.Sprintf("%v", int(village.Storage["1"].(float64)))+"/"+fmt.Sprintf("%v", village.StorageCapacity["1"])+" "+fmt.Sprintf("%v", int(village.Storage["2"].(float64)))+"/"+fmt.Sprintf("%v", village.StorageCapacity["2"])+" "+fmt.Sprintf("%v", int(village.Storage["3"].(float64)))+"/"+fmt.Sprintf("%v", village.StorageCapacity["3"])+" "+fmt.Sprintf("%v", int(village.Storage["4"].(float64)))+"/"+fmt.Sprintf("%v", village.StorageCapacity["4"])),
					// resbar,
				))
				for _, structs := range structures {
					if structs.VillageID == village.VillageID {
						for _, building := range structs.CollectionBuildings {
							dataLabels.Append(tui.NewVBox(
								tui.NewLabel("Field name: " + getall.StructuresMap[building.Data.BuildingType] + "(" + building.Data.BuildingType + ")" + " Location: " + building.Data.LocationID + " Level: " + building.Data.Lvl + " NxtLvLRes: " + fmt.Sprintf("%v", building.Data.UpgradeCosts["1"]) + "/" + fmt.Sprintf("%v", building.Data.UpgradeCosts["2"]) + "/" + fmt.Sprintf("%v", building.Data.UpgradeCosts["3"]) + "/" + fmt.Sprintf("%v", building.Data.UpgradeCosts["4"])),
							))
						}
					}
				}
			}
			heroLabels.Append(tui.NewVBox(
				tui.NewLabel("Hero is in village: "+herodata.VillageID),
				tui.NewLabel("Hero health: "+fmt.Sprintf("%v", herodata.Health)),
				tui.NewLabel("Hero Adventure Points: "+fmt.Sprintf("%v", herodata.AdventurePoints)),
				tui.NewLabel("Hero is moving: "+fmt.Sprintf("%v", herodata.IsMoving)),
				tui.NewLabel("Attack detected: "+strings.Join(attacks, ",")),
			))
			writelog(history, fmt.Sprintf("AutoAdventure module starting..."))
			hadvp, err := strconv.Atoi(herodata.AdventurePoints)
			if err != nil {
				writelog(history, fmt.Sprintf("herodata.AdventurePoints convertion error: %s", err))
			}
			switch herodata.Health.(type) {
			case string:
				health, err := strconv.ParseFloat(herodata.Health.(string), 64)
				if err != nil {
					writelog(history, fmt.Sprintf("herodata.Health convertion from string to float64 error: %s", err))
				}
				if hadvp > 0 && !herodata.IsMoving && int(health) >= 15 && hadvp > 0 {
					err := startAdventure()
					if err != nil {
						writelog(history, fmt.Sprintf("AutoAdventure module error: %s", err))
					} else {
						writelog(history, fmt.Sprintf("AutoAdventure module done with success."))
					}
				} else {
					writelog(history, fmt.Sprintf("Hero is not ready for adventure."))
				}
			case float64:
				health := herodata.Health.(float64)
				if hadvp > 0 && !herodata.IsMoving && int(health) >= 15 && hadvp > 0 {
					err := startAdventure()
					if err != nil {
						writelog(history, fmt.Sprintf("AutoAdventure module error: %s", err))
					} else {
						writelog(history, fmt.Sprintf("AutoAdventure module done with success."))
					}
				} else {
					writelog(history, fmt.Sprintf("Hero is not ready for adventure."))
				}
			}

			fldatalist, err := farmListLoader()
			if err != nil {
				writelog(history, fmt.Sprintf("farmListLoader module error: %s", err))
			}
			for _, fldata := range fldatalist {
				flList.Append(tui.NewVBox(
					tui.NewLabel("FL Name: "+fldata["listName"].(string)),
					tui.NewSpacer(),
				))
			}
			dataLabelslength = dataLabels.Length()
			flListlength = flList.Length()
			heroLabelslength = heroLabels.Length()
			time.Sleep(dataloadperiod)
		}
	}
}

func startAdventure() error {
	var getadvopt bot.ReqOptions
	getadvopt.Req.Method = "POST"
	getadvopt.Req.Data.Session = token
	getadvopt.Req.Data.Action = "dialogAction"
	getadvopt.Req.Data.Controller = "quest"
	getadvopt.Req.Data.Params = map[string]interface{}{"questId": 991, "dialogId": 0, "command": "activate"}
	err := bot.AutoAdventures(getadvopt, token, server)
	if err != nil {
		return err
	}
	return nil
}

func loginUser(history *tui.Box, userserver, username, password *tui.Entry) error {
	writelog(history, fmt.Sprintf("Trying to login as %v on server %v", username.Text(), userserver.Text()))

	return nil
}

func farmListLoader() ([]map[string]interface{}, error) {
	var getflopt bot.ReqOptions
	var fldatalist []map[string]interface{}
	getflopt.Req.Method = "POST"
	getflopt.Req.Data.Session = token
	getflopt.Req.Data.Action = "get"
	getflopt.Req.Data.Controller = "cache"
	getflopt.Req.Data.Params = map[string]interface{}{"names": []string{"Collection:FarmList:"}}
	cacheitems, err := bot.GetFL(getflopt, token, server)
	if err != nil {
		return nil, err
	}
	for _, cacheitem := range cacheitems {
		if cacheitem.(map[string]interface{})["data"].(map[string]interface{})["cache"] != nil {
			for _, i := range cacheitem.(map[string]interface{})["data"].(map[string]interface{})["cache"].([]interface{}) {
				fldata := i.(map[string]interface{})["data"].(map[string]interface{})
				fldatalist = append(fldatalist, fldata)
			}
		}
	}
	return fldatalist, nil
}
