package getall

type RequestOptions struct {
	Method string
	Data   struct {
		Controller string                 `json:"controller,omitempty"`
		Action     string                 `json:"action,omitempty"`
		Params     map[string]interface{} `json:"params,omitempty"`
		Session    string                 `json:"session,omitempty"`
	}
}

type AllBuildings struct {
	VillageID           string `json:"villageId"`
	CollectionBuildings []CollectionBuilding
}

type CollectionBuilding struct {
	Name string
	Data BuildingData
}

type BuildingData struct {
	BuildingType string                 `json:"buildingType"`
	Category     int                    `json:"category"`
	Effect       []interface{}          `json:"effect"`
	IsMaxLvl     bool                   `json:"isMaxLvl"`
	LocationID   string                 `json:"locationId"`
	Lvl          string                 `json:"lvl"`
	LvlMax       int                    `json:"lvlMax"`
	LvlNext      string                 `json:"lvlNext"`
	UpgradeCosts map[string]interface{} `json:"upgradeCosts"`
	UpgradeTime  int                    `json:"upgradeTime"`
	VillageID    string                 `json:"villageId"`
}

type PlayerData struct {
	Active                        string      `json:"active"`
	AvatarIdentifier              string      `json:"avatarIdentifier"`
	BannedFromMessaging           string      `json:"bannedFromMessaging"`
	BrewCelebration               string      `json:"brewCelebration"`
	CoronationDuration            int         `json:"coronationDuration"`
	CropProductionBonusTime       string      `json:"cropProductionBonusTime"`
	DailyQuestsExchanged          interface{} `json:"dailyQuestsExchanged"`
	DeletionTime                  string      `json:"deletionTime"`
	FilterInformation             bool        `json:"filterInformation"`
	Gold                          string      `json:"gold"`
	HasNoobProtection             bool        `json:"hasNoobProtection"`
	HintStatus                    string      `json:"hintStatus"`
	IsActivated                   string      `json:"isActivated"`
	IsBannedFromMessaging         bool        `json:"isBannedFromMessaging"`
	IsInstant                     string      `json:"isInstant"`
	IsKing                        bool        `json:"isKing"`
	IsPunished                    bool        `json:"isPunished"`
	KingID                        interface{} `json:"kingId"`
	KingdomID                     string      `json:"kingdomId"`
	KingdomRole                   string      `json:"kingdomRole"`
	KingdomTag                    string      `json:"kingdomTag"`
	Kingstatus                    string      `json:"kingstatus"`
	LastPaymentTime               string      `json:"lastPaymentTime"`
	Level                         int         `json:"level"`
	Limitation                    string      `json:"limitation"`
	LimitationFlags               string      `json:"limitationFlags"`
	LimitedPremiumFeatureFlags    string      `json:"limitedPremiumFeatureFlags"`
	Name                          string      `json:"name"`
	NextDailyQuestTime            int         `json:"nextDailyQuestTime"`
	NextLevelPrestige             int         `json:"nextLevelPrestige"`
	PlayerID                      string      `json:"playerId"`
	PlusAccountTime               string      `json:"plusAccountTime"`
	Population                    string      `json:"population"`
	PremiumFeatureAutoExtendFlags string      `json:"premiumFeatureAutoExtendFlags"`
	Prestige                      int         `json:"prestige"`
	ProductionBonusTime           string      `json:"productionBonusTime"`
	QuestVersion                  string      `json:"questVersion"`
	SignupTime                    string      `json:"signupTime"`
	Silver                        string      `json:"silver"`
	SpawnedOnMap                  string      `json:"spawnedOnMap"`
	Stars                         struct {
		Bronze int `json:"bronze"`
		Gold   int `json:"gold"`
		Silver int `json:"silver"`
	} `json:"stars"`
	TribeID            string `json:"tribeId"`
	UILimitations      string `json:"uiLimitations"`
	UIStatus           string `json:"uiStatus"`
	UsedVacationDays   string `json:"usedVacationDays"`
	VacationState      string `json:"vacationState"`
	VacationStateEnd   string `json:"vacationStateEnd"`
	VacationStateStart string `json:"vacationStateStart"`
	Villages           []struct {
		Acceptance             int         `json:"acceptance"`
		AcceptanceProduction   string      `json:"acceptanceProduction"`
		AllowTributeCollection string      `json:"allowTributeCollection"`
		AvailableControlPoints string      `json:"availableControlPoints"`
		BelongsToKing          string      `json:"belongsToKing"`
		BelongsToKingdom       interface{} `json:"belongsToKingdom"`
		CelebrationEnd         string      `json:"celebrationEnd"`
		CelebrationType        string      `json:"celebrationType"`
		Coordinates            struct {
			X string `json:"x"`
			Y string `json:"y"`
		} `json:"coordinates"`
		CulturePointProduction   string                 `json:"culturePointProduction"`
		CulturePoints            interface{}            `json:"culturePoints"`
		EstimatedWarehouseLevel  int                    `json:"estimatedWarehouseLevel"`
		IsMainVillage            bool                   `json:"isMainVillage"`
		IsTown                   bool                   `json:"isTown"`
		Name                     string                 `json:"name"`
		PlayerID                 string                 `json:"playerId"`
		Population               string                 `json:"population"`
		Production               map[string]interface{} `json:"production"`
		ProtectionGranted        string                 `json:"protectionGranted"`
		RealTributePercent       float64                `json:"realTributePercent"`
		Storage                  map[string]interface{} `json:"storage"`
		StorageCapacity          map[string]interface{} `json:"storageCapacity"`
		SupplyBuildings          string                 `json:"supplyBuildings"`
		SupplyTroops             interface{}            `json:"supplyTroops"`
		TreasureResourceBonus    string                 `json:"treasureResourceBonus"`
		Treasures                string                 `json:"treasures"`
		TreasuresUsable          string                 `json:"treasuresUsable"`
		Treasury                 map[string]interface{} `json:"treasury"`
		TribeID                  string                 `json:"tribeId"`
		TributeCollectorPlayerID int                    `json:"tributeCollectorPlayerId"`
		TributeProduction        int                    `json:"tributeProduction"`
		TributeProductionDetail  map[string]interface{} `json:"tributeProductionDetail"`
		TributeTime              string                 `json:"tributeTime"`
		TributeTreasures         int                    `json:"tributeTreasures"`
		Tributes                 map[string]interface{} `json:"tributes"`
		TributesCapacity         string                 `json:"tributesCapacity"`
		TributesRequiredToFetch  int                    `json:"tributesRequiredToFetch"`
		Type                     string                 `json:"type"`
		UsedControlPoints        string                 `json:"usedControlPoints"`
		VillageID                string                 `json:"villageId"`
	} `json:"villages"`
}

type HeroData struct {
	AdventurePointCardUsedToday string      `json:"adventurePointCardUsedToday"`
	AdventurePointTime          string      `json:"adventurePointTime"`
	AdventurePoints             string      `json:"adventurePoints"`
	ArtworkUsedToday            string      `json:"artworkUsedToday"`
	AttBonusPoints              string      `json:"attBonusPoints"`
	BaseRegenerationRate        int         `json:"baseRegenerationRate"`
	Bonuses                     interface{} `json:"bonuses"`
	CropChestsUsedToday         string      `json:"cropChestsUsedToday"`
	DefBonusPoints              string      `json:"defBonusPoints"`
	DestVillageID               string      `json:"destVillageId"`
	FightStrength               int         `json:"fightStrength"`
	FightStrengthPoints         string      `json:"fightStrengthPoints"`
	FreePoints                  string      `json:"freePoints"`
	Health                      interface{} `json:"health"`
	IsMoving                    bool        `json:"isMoving"`
	LastHealthTime              string      `json:"lastHealthTime"`
	Level                       string      `json:"level"`
	LevelUp                     string      `json:"levelUp"`
	MaxScrollsPerDay            string      `json:"maxScrollsPerDay"`
	OintmentsUsedToday          string      `json:"ointmentsUsedToday"`
	PlayerID                    string      `json:"playerId"`
	RegenerationRate            int         `json:"regenerationRate"`
	ResBonusPoints              string      `json:"resBonusPoints"`
	ResBonusType                string      `json:"resBonusType"`
	ResourceChestsUsedToday     string      `json:"resourceChestsUsedToday"`
	ScrollsUsedToday            string      `json:"scrollsUsedToday"`
	Speed                       int         `json:"speed"`
	Status                      interface{} `json:"status"`
	UntilTime                   string      `json:"untilTime"`
	VillageID                   string      `json:"villageId"`
	WaterbucketUsedToday        string      `json:"waterbucketUsedToday"`
	Xp                          string      `json:"xp"`
	XpNextLevel                 int         `json:"xpNextLevel"`
	XpThisLevel                 int         `json:"xpThisLevel"`
}

// type MovingUnits struct {
// 	Data struct {
// 		Capacity int    `json:"capacity"`
// 		Filter   string `json:"filter"`
// 		Movement struct {
// 			CatapultTarget1 interface{}            `json:"catapultTarget1"`
// 			CatapultTarget2 interface{}            `json:"catapultTarget2"`
// 			CoordinateID    int                    `json:"coordinateID"`
// 			Merchants       string                 `json:"merchants"`
// 			MovementType    string                 `json:"movementType"`
// 			PlayerIDTarget  string                 `json:"playerIdTarget"`
// 			Resources       map[string]interface{} `json:"resources"`
// 			SpyTarget       interface{}            `json:"spyTarget"`
// 			TimeFinish      string                 `json:"timeFinish"`
// 			TimeStart       interface{}            `json:"timeStart"`
// 			Treasures       string                 `json:"treasures"`
// 			TroopID         interface{}            `json:"troopId"`
// 			VillageIDStart  string                 `json:"villageIdStart"`
// 			VillageIDTarget string                 `json:"villageIdTarget"`
// 		} `json:"movement"`
// 		PlayerID            string      `json:"playerId"`
// 		PlayerIDLocation    string      `json:"playerIdLocation"`
// 		PlayerName          string      `json:"playerName"`
// 		PlayerNameLocation  string      `json:"playerNameLocation"`
// 		Status              string      `json:"status"`
// 		SupplyTroops        interface{} `json:"supplyTroops"`
// 		TribeID             string      `json:"tribeId"`
// 		TroopID             interface{} `json:"troopId"`
// 		Units               interface{} `json:"units"`
// 		VillageID           string      `json:"villageId"`
// 		VillageIDLocation   string      `json:"villageIdLocation"`
// 		VillageIDSupply     string      `json:"villageIdSupply"`
// 		VillageName         interface{} `json:"villageName"`
// 		VillageNameLocation string      `json:"villageNameLocation"`
// 	} `json:"data"`
// 	Name string `json:"name"`
// }

var StructuresMap = map[string]string{
	"1":  "wood",
	"2":  "clay",
	"3":  "iron",
	"4":  "crop",
	"5":  "sawmill",
	"6":  "brickworks",
	"7":  "foundry",
	"8":  "mill",
	"9":  "bakery",
	"10": "storehouse",
	"11": "granary",
	"13": "forge",
	"15": "main",
	"16": "colpoint",
	"17": "market",
	"18": "tavern",
	"19": "barracks",
	"20": "stable",
	"21": "workshop",
	"22": "academy",
	"23": "cranny",
	"24": "townhall",
	"25": "residence",
	"32": "wall",
}

var Units = map[string]string{
	"1":  "Legioner",
	"2":  "Pretorian",
	"3":  "Imperian",
	"4":  "Scouts",
	"5":  "Imperator",
	"6":  "Ceserian",
	"10": "Settler",
	"11": "Clubswinger",
	"12": "Spearfighter",
	"13": "Axefighter",
	"14": "Scout",
	"15": "Paladin",
	"16": "Teutonic knight",
	"21": "Phalanx",
	"22": "Swordsman",
	"23": "Scout",
	"24": "Thunder T",
	"25": "Druids",
	"26": "Eduins",
}
